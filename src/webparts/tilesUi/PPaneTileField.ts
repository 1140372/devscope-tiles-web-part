import * as React from 'react';
import * as ReactDom from 'react-dom';
import {
    IPropertyPaneField,
    PropertyPaneFieldType,
    IPropertyPaneCustomFieldProps
} from '@microsoft/sp-webpart-base';
import PPTileEditorField from './components/PPTileEditorField/PPTileEditorField';
import { IPPTileFieldProps } from './components/PPTileEditorField/IPPTileFieldProps';


export default class PPaneTileField implements IPropertyPaneField<IPropertyPaneCustomFieldProps>{
    public type: PropertyPaneFieldType = PropertyPaneFieldType.Custom;
    public targetProperty: string;
    public properties: any;

    private currentTile;
    private propertyChanged;
    private onSubmit;

    public constructor(properties: any) {
        this.properties = properties;
        this.properties.onRender = this.render.bind(this);
        this.currentTile = this.properties.selectedTile.tile;
        this.propertyChanged = this.properties.propertyChanged;
        this.onSubmit = this.properties.onSubmit;
    }

    public render(elem: HTMLElement) {
        const element: React.ReactElement<IPPTileFieldProps> = React.createElement(PPTileEditorField, {
            tile: this.currentTile,
            edit: this.properties.selectedTile.edit,
            btnLabel: this.properties.type,
            propertyChanged: this.propertyChanged,
            onSubmit: this.onSubmit
        });
        ReactDom.render(element, elem);
    }
}

