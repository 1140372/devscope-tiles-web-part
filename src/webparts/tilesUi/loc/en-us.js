define([], function () {
  return {
    "spContentTypeID": "0x010500E49CEE0C16064ABBBD9FFB85DE04BDCC",
    "standartListName": "TileList",
    "PPCancelBTN": "Cancel",

    //PropertyPane -  Base
    "PPBaseGroup": "Web Part Settings",
    "PPPanelTitle": "Panel Title",
    "PPBgColor": "Background color:",
    "PPSelectList": "Tile list:",
    "PPCreateListBTN": "Create list",

    //PropertyPane - Tile Editor
    "PPTileEditor": "Tile Editor",
    "PPTileTitle": "Title:",
    "PPTileDescription": "Description:",
    "PPTileLinkURL": "Link URL:",
    "PPTileFormFactor": "Form Factor:",
    "PPTileTypePicker": "Type:",
    "PPTileIconPicker": "Icon:",
    "PPTileColorPicker": "Background color:",
    "PPTileImageURL": "Image URL:",
    "PPTileCreateBTN": "Create Tile",
    "PPTileUpdateBTN": "Update Tile",
    "PPTileSelectPicList": "Select a picture library:",

    //PropertyPane - List Editor
    "PPListEditor": "List Editor:",
    "PPListTitle": "Title:",
    "PPListDescription": "Description:",

    //Tile Content
    "TileTypeImage": "Image",
    "TileTypeIcon": "Icon",
    "TileTypeAnimated": "Animated",
    "TileTypeLive": "Live",

    "TileFormFactorHalf": "Mini",
    "TileFormFactorDefault": "Base",
    "TileFormFactorDouble": "Double",
    "TileFormFactorTriple": "Triple",
    "TileFormFactorQuadro": "Quadruple",

    "TileAnimationSlideLeft": "Slide to the left",
    "TileAnimationSlideRight": "Slide to the right",
    "TileAnimationSlideLeftRight": "Slide left and right",
    "TileAnimationSlideUp": "Slide up",
    "TileAnimationSlideDown": "Slide down",
    "TileAnimationSlideUpDown": "Slide up and down",

    "LoadingMessage": "Loading..."
  }
});
