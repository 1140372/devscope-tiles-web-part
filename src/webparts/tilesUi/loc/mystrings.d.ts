declare interface ITilesUiStrings {
  spContentTypeID: string;
  standartListName: string;
  PPCancelBTN: string;

  //PropertyPane -  Base
  PPBaseGroup: string;
  PPPanelTitle: string;
  PPBgColor: string;
  PPSelectList: string;
  PPCreateListBTN: string;

  //PropertyPane - Tile Editor
  PPTileEditor: string;
  PPTileTitle: string;
  PPTileDescription: string;
  PPTileLinkURL: string;
  PPTileFormFactor: string;
  PPTileTypePicker: string;
  PPTileIconPicker: string;
  PPTileColorPicker: string;
  PPTileImageURL: string;
  PPTileCreateBTN: string;
  PPTileUpdateBTN: string;
  PPTileSelectPicList: string;

  //PropertyPane - List Editor
  PPListEditor: string;
  PPListTitle: string;
  PPListDescription: string;

  //Tile Content
  TileTypeImage: string;
  TileTypeIcon: string;
  TileTypeAnimated: string;
  TileTypeLive: string;

  TileFormFactorHalf: string;
  TileFormFactorDefault: string;
  TileFormFactorDouble: string;
  TileFormFactorTriple: string;
  TileFormFactorQuadro: string;

  TileAnimationSlideLeft: string;
  TileAnimationSlideRight: string;
  TileAnimationSlideLeftRight: string;
  TileAnimationSlideUp: string;
  TileAnimationSlideDown: string;
  TileAnimationSlideUpDown: string;

  LoadingMessage: string;
}

declare module 'tilesUiStrings' {
  const strings: ITilesUiStrings;
  export = strings;
}
