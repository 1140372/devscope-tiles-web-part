define([], function () {
  return {
    "spContentTypeID": "0x010500E49CEE0C16064ABBBD9FFB85DE04BDCC",
    "standartListName": "Lista de Tiles",
    "PPCancelBTN": "Cancelar",

    //PropertyPane -  Base
    "PPBaseGroup": "Definições da WebPart",
    "PPPanelTitle": "Titulo do Painel:",
    "PPBgColor": "Cor de fundo:",
    "PPSelectList": "Lista de tiles:",
    "PPCreateListBTN": "Criar Lista",

    //PropertyPane - Tile Editor
    "PPTileEditor": "Editor de Tiles",
    "PPTileTitle": "Titulo:",
    "PPTileDescription": "Descrição:",
    "PPTileLinkURL": "URL do link:",
    "PPTileFormFactor": "Formato:",
    "PPTileTypePicker": "Tipo:",
    "PPTileIconPicker": "Icon:",
    "PPTileColorPicker": "Cor:",
    "PPTileImageURL": "URL da imagem:",
    "PPTileCreateBTN": "Criar Tile",
    "PPTileUpdateBTN": "Atualizar Tile",
    "PPTileSelectPicList": "Seleciona uma biblioteca de imagens:",

    //PropertyPane - List Editor 
    "PPListEditor": "Editor de listas:",
    "PPListTitle": "Titulo:",
    "PPListDescription": "Descrição:",

    //Tile Content
    "TileTypeImage": "Imagem",
    "TileTypeIcon": "Ícone",
    "TileTypeAnimated": "Animado",
    "TileTypeLive": "Live",

    "TileFormFactorHalf": "Mini",
    "TileFormFactorDefault": "Base",
    "TileFormFactorDouble": "Dobro",
    "TileFormFactorTriple": "Triplo",
    "TileFormFactorQuadro": "Quadruplo",

    "TileAnimationSlideLeft": "Desliza para a esquerda",
    "TileAnimationSlideRight": "Desliza para a direita",
    "TileAnimationSlideLeftRight": "Desliza direita e esquerda",
    "TileAnimationSlideUp": "Desliza para cima",
    "TileAnimationSlideDown": "Desliza para baixo",
    "TileAnimationSlideUpDown": "Desliza para cima e para baixo",

    "LoadingMessage": "Carregando..."
  }
});
