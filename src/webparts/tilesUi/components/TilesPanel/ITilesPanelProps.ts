export interface ITilesPanelProps {
  tiles: any;
  title: string;
  bcolor: string;
  editMode: boolean;
  selectedList: string;
  action: (props: any, target: string) => void;
}
