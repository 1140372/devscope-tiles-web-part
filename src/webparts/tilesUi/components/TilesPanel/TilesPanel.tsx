import { Button, CommandButton } from 'office-ui-fabric-react';
import * as React from 'react';
import { ITilesPanelProps } from './ITilesPanelProps';
import Tile from '../Tile/Tile';

export default class TilesPanel extends React.Component<ITilesPanelProps, void> {

  public render(): React.ReactElement<ITilesPanelProps> {
    return (
      <div>
        <link href="https://devscopespfx.blob.core.windows.net/tiles/jquery.min.js"></link>
        <link href="https://devscopespfx.blob.core.windows.net/tiles/jquery.widget.min.js"></link>
        <link href="https://devscopespfx.blob.core.windows.net/tiles/jquery.mousewheel.js"></link>
        <link href="https://devscopespfx.blob.core.windows.net/tiles/jquery.easing.1.3.min.js"></link>
        <link href="https://devscopespfx.blob.core.windows.net/tiles/metro.min.js"></link>
        
        
        <div className="metro">
          <div className={"tile-area bg-" + this.props.bcolor + " box"}>
            <h1 className="title">{this.props.title}</h1>
            {this._formatTiles()}
            {this.props.editMode &&
              <div>
                <CommandButton
                  icon='Add'
                  onClick={() => this.props.action({ edit: false, tile: {} }, 'Tile')}>
                  Create tile
              </CommandButton>
              </div>}
          </div>
        </div>

      </div>
    );
  }

  private _formatTiles() {
    let tiles: any[] = [];
    if (this.props.tiles != null)
      for (var i = 0; i < this.props.tiles.length; i++) {
        tiles.push(
          <Tile
            tile={this.props.tiles[i]}
            action={this.props.action}
            edit={this.props.editMode}
            delete={false}
          />
        );
      }
    return tiles;
  }
}



