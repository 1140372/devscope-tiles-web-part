import * as React from 'react';
import pnp from 'sp-pnp-js';
import { IPPTileFieldProps } from './IPPTileFieldProps';
import { TextField, Toggle, PrimaryButton, ColorPicker, Image, Button } from 'office-ui-fabric-react';
import PPCustomDropDown from '../PPCustomDropDown/PPaneCustomDropDown';
import { ITile, formFactor, animation, tileType } from '../../Model/ITile';
import { icons } from '../../icons';
import { colors } from '../../colors';
import cellRender from '../PPCustomDropDown/CustomCellRenders';
import * as strings from 'tilesUiStrings';



export interface IPPPaneFieldState {
    tile: ITile;
    btnLabel: string;
    previewImage: boolean;
    libraries?: any;
}
export default class PPTileEditorField extends React.Component<IPPTileFieldProps, IPPPaneFieldState>{

    constructor(props) {
        super(props);
        this._setupState(this.props);
        this._getPictureLibraries();
    }

    public componentWillReceiveProps(nextProps) {
        debugger;
        this._setupState(nextProps);
        this.setState(this.state);
    }

    public render() {
        return (
            <div>
                <TextField // Tile Title
                    label={strings.PPTileTitle}
                    value={this.state.tile.Label}
                    onChanged={this._setTitle.bind(this)} />
                <TextField // Tile description
                    label={strings.PPTileDescription}
                    multiline rows={4}
                    value={this.state.tile.URL.Description}
                    onChanged={this._setDescription.bind(this)} />
                <TextField // Tile Link
                    label={strings.PPTileLinkURL}
                    value={this.state.tile.URL.Url}
                    onChanged={this._setURL.bind(this)} />
                <PPCustomDropDown //List of tile formFactors
                    label={strings.PPTileFormFactor}
                    dropDownOptions={formFactor.getOptionList()}
                    onSelectAction={this._setFormFactor.bind(this)}
                    selectedItem={this.state.tile.FormFactor.toString()} />
                <PPCustomDropDown //List of tile types
                    label={strings.PPTileTypePicker}
                    dropDownOptions={tileType.getOptionList()}
                    onSelectAction={this._setTileType.bind(this)}
                    selectedItem={this.state.tile.Type.toString()} />
                {this.renderTileTypeOptions()}
                <PrimaryButton // Submit button
                    onClick={this._submit.bind(this)}>{this.state.btnLabel}</PrimaryButton>
                <Button //Cancel Button
                    onClick={this._dismiss.bind(this)}>{strings.PPCancelBTN}</Button>
            </div>
        );
    }

    private renderTileTypeOptions() {
        switch (this.state.tile.Type.toString().charAt(0).toUpperCase() + this.state.tile.Type.toString().slice(1)) { //REVER
            case tileType.IconTile.toString():
                return (<div>
                    <PPCustomDropDown
                        key={tileType.IconTile.toString()}
                        label={strings.PPTileIconPicker}
                        dropDownOptions={icons}
                        onSelectAction={this._setIcon.bind(this)}
                        selectedItem={this.state.tile.Content}
                        renderItem={cellRender.renderIconOption}
                        renderSelectedItem={cellRender.renderIconOption} />
                    <PPCustomDropDown
                        key={tileType.IconTile.toString()+"1"}
                        label={strings.PPTileColorPicker}
                        dropDownOptions={colors}
                        onSelectAction={this._setColor.bind(this)}
                        selectedItem={this.state.tile.Color}
                        renderItem={cellRender.renderColorOption}
                        renderSelectedItem={cellRender.renderColorOption} />
                </div>);

            case tileType.ImageTile.toString():
                return (<div>
                    <PPCustomDropDown
                        key={tileType.ImageTile.toString()}
                        label={strings.PPTileColorPicker}
                        dropDownOptions={colors}
                        onSelectAction={this._setColor.bind(this)}
                        selectedItem={this.state.tile.Color}
                        renderItem={cellRender.renderColorOption}
                        renderSelectedItem={cellRender.renderColorOption} />
                    <TextField
                        key={tileType.ImageTile.toString()+"1"}
                        label={strings.PPTileImageURL}
                        value={this.state.tile.Content}
                        onChanged={this._setImageURL.bind(this)} />
                    {this.state.previewImage && // Renders the image from the URL if there is a loaded URL
                        <Image src={this.state.tile.Content} alt='No image loaded' width={100} height={100} />}
                </div>);
            case tileType.AnimatedTile.toString():
                return (
                    <div>
                        <PPCustomDropDown
                            key={tileType.AnimatedTile.toString()}
                            label={'Animation:'}
                            dropDownOptions={animation.getOptionList()}
                            onSelectAction={this._setAnimation.bind(this)}
                            selectedItem={this.state.tile.Animation.toString()} />
                        <PPCustomDropDown //List of tile formFactors
                            key={tileType.AnimatedTile.toString()+"1"}
                            label={strings.PPTileSelectPicList}
                            dropDownOptions={this.state.libraries}
                            onSelectAction={this._setPictureLibrary.bind(this)}
                            selectedItem={this.state.tile.FormFactor.toString()} />
                    </div>);
            case tileType.LiveTile.toString():
                return;
        }
    }

    private _setupState(props: IPPTileFieldProps) {
        let tile: ITile = props.tile;
        if (!props.edit) {
            tile = {
                Label: '',
                Content: '',
                Ativo: false,
                Live: false,
                Color: '',
                PictureLibId: '',
                FormFactor: formFactor.default.toString(),
                Animation: animation.slideUp.toString(),
                Type: tileType.IconTile.toString(),
                URL: {
                    Description: '',
                    Url: ''
                }
            };
        }
        this.state = {
            tile: tile,
            btnLabel: props.btnLabel,
            previewImage: true, // REVER
        };

    }

    private _submit(): void {
        this.props.onSubmit(this.state.tile, this.state.btnLabel);
    }

    private _setTitle(input: string): void {
        this.state.tile.Label = input;
    }

    private _setDescription(input: string): void {
        this.state.tile.URL.Description = input;
    }

    private _setURL(input: string): void {
        this.state.tile.URL.Url = input;
    }

    private _setImageURL(input: string): void {
        this.state.tile.Content = input;
        this.state.previewImage = this._validateImageURL(input);
        this.setState(this.state);
    }

    private _setFormFactor(input): void {
        this.state.tile.FormFactor = input;
    }

    private _setAnimation(input): void {
        this.state.tile.Animation = input;
    }

    private _setColor(input): void {
        this.state.tile.Color = input;
    }

    private _setIcon(input): void {
        this.state.tile.Content = input;
    }

    private _setTileType(input): void {
        switch (input) {
            case tileType.AnimatedTile.toString():
                this.state.tile.Ativo = true;
                this.state.tile.Live = false;
                break;
            case tileType.LiveTile.toString():
                this.state.tile.Live = true;
                this.state.tile.Ativo = false;
                break;
        }
        this.state.tile.Type = input;
        this.setState(this.state);
    }

    private _setPictureLibrary(input): void {
        this.state.tile.PictureLibId = input;
    }

    private _dismiss() {
        this.props.onSubmit();
    }
    //UTILS/////////////////////////////////////////////////////////////////////////////////////////////////////////

    private _validateImageURL(url: string): boolean {
        return (url.match(/\.(jpeg|jpg|gif|png)$/) != null);
    }

    private _getPictureLibraries() {
        this.state.libraries = [];
        pnp.sp.web.lists.filter('BaseTemplate eq 109').get().then((libraries) => {
            for (var i = 0; i < libraries.length; i++) {
                this.state.libraries.push({
                    key: libraries[i].Id,
                    text: libraries[i].Title
                });
            }
        });
    }
}