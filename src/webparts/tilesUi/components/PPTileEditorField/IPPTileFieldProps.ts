import {ITile} from '../../Model/ITile';

export interface IPPTileFieldProps{
    tile: ITile;
    btnLabel: string;
    edit: boolean;
    propertyChanged: (params: any) => void;
    onSubmit: (params?: any, action?:string) => void;
}