import * as React from 'react';
import { IPCustomDropDownProps } from './PPaneCDropDownProps';
import { Dropdown, IDropdownOption} from 'office-ui-fabric-react';

export interface PPCustomDropDownState {
    label: string;
    dropDownOptions: any;
    selectedItem: any;
    action: any;
    renderSelectedItem: (props?: IDropdownOption)=> JSX.Element;
    renderItem:  (props?: IDropdownOption)=> JSX.Element;
}

export default class PPCustomDropDown extends React.Component<IPCustomDropDownProps, PPCustomDropDownState>{

    constructor(props) {
        super(props);
        this.state = {
            label: this.props.label,
            dropDownOptions: this.props.dropDownOptions,
            selectedItem: this.props.selectedItem,
            action: this.props.onSelectAction,
            renderSelectedItem: this.props.renderSelectedItem,
            renderItem: this.props.renderItem
        };
    }

    public componentWillReceiveProps(nextProps: IPCustomDropDownProps){
        this.state.dropDownOptions = nextProps.dropDownOptions;
        this.setState(this.state);
    }

    public render() {
        return (
            <div>
                <Dropdown
                    label={this.state.label}
                    options={this.state.dropDownOptions}
                    onChanged={this._handleChange.bind(this)}
                    selectedKey={this.state.selectedItem}
                    onRenderOption={(this.state.dropDownOptions, this.state.renderItem)}
                    onRenderTitle={(this.state.selectedItem, this.state.renderSelectedItem)}>
                </Dropdown>
            </div>
        );
    }

    private _handleChange(item: any) {
        this.state.selectedItem = item.key;
        this.setState(this.state);
        this.state.action(item.key);
    }
}