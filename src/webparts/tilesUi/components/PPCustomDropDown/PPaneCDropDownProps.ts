import {IDropdownOption} from 'office-ui-fabric-react';

export interface IPCustomDropDownProps {
    label: string;
    dropDownOptions: any;
    selectedItem: string;
    onSelectAction: (params: any) => void;
    renderSelectedItem?: (props?: IDropdownOption)=> JSX.Element;
    renderItem?:  (props?: IDropdownOption)=> JSX.Element;
}