import * as React from 'react';
import pnp from 'sp-pnp-js';
import { ITile, tileType } from '../../Model/ITile';
import { ITileProps } from './ITileProps';
import {
    Button,
    IconButton,
    ButtonType,
    Image,
    CommandButton,
    DialogType,
    Dialog,
    DialogFooter
} from 'office-ui-fabric-react';



export interface ITileState {
    tile: ITile;
    edit: boolean;
    delete: boolean;
    images: any[];
    selected: boolean;
}

export default class Tile extends React.Component<ITileProps, ITileState>{

    public constructor(props) {
        super(props);
        this.state = { ...props };
        this.state.selected = false;
        this.state.images = [];
        this._loadImages();
    }

    public componentWillReceiveProps(nextProps) {
                debugger;
        
        this.state = { ...nextProps };
        this._loadImages();
    }

    public render() {
        debugger;
        if (this.state.edit)
            return (
                <div>
                    {this._defineTileActionIcons()}
                    <div className={this._defineTileType()}
                        data-role={this.state.tile.Ativo && "live-tile"}
                        data-effect={this.state.tile.Ativo && this.state.tile.Animation}
                        onClick={this._toggleClick.bind(this)}>
                        <div className={this._defineTileContentType()}>
                            {this._defineTileContent()}
                            {this._defineDeletePopUp()}
                        </div>
                        {this._defineTileBrand()}
                    </div>
                </div>);
        else {
            return (
                <div>
                    {this._defineTileActionIcons()}
                    <a href={this.state.tile.URL.Url} target="_blank">
                        <div className={this._defineTileType()}
                            data-role={this.state.tile.Ativo && "live-tile"}
                            data-effect={this.state.tile.Ativo && this.state.tile.Animation}
                            onClick={this._toggleClick.bind(this)}>
                            <div className={this._defineTileContentType()}>
                                {this._defineTileContent()}
                                {this._defineDeletePopUp()}
                            </div>
                            {this._defineTileBrand()}
                        </div>
                    </a>
                </div>);
        }
    }

    private _edit() {
        this.props.action(this.state, 'Tile Edit');
    }

    private _delete() {
        if (this.state.delete) {
            this.state.delete = !this.state.delete;
            this.props.action(this.state, 'Delete');
            this.setState(this.state);
        }
    }

    private _showDeleteDialog() {
        this.state.delete = !this.state.delete;
        this.setState(this.state);
    }

    //BUILDING BLOCKS FOR THE TILE COMPONENT//////////////////////////////////////////////////////////////////////////////
    private _defineTileType(): string {
        let cssClass: string = "tile " + this.state.tile.FormFactor.toString() + " bg-" + this.state.tile.Color;
        if (this.state.tile.Ativo)
            cssClass += ' live';
        return cssClass;
    }

    private _defineTileContentType(): string {
        switch (this.props.tile.Type.toString()) {
            case tileType.IconTile.toString():
                return 'tile-content-icon';
            case tileType.AnimatedTile.toString():
                return;
            default:
                return 'tile-content image';
        }
    }

    private _defineTileContent(): any {
        switch (this.props.tile.Type.toString()) {
            case tileType.IconTile.toString():
                return <i className={"ms-Icon ms-Icon--" + this.state.tile.Content} />;
            case tileType.ImageTile.toString():
                return <img src={this.state.tile.Content} alt='Image not loading...' href='#' />;
            case tileType.AnimatedTile.toString():
                return (this.state.images);
            case tileType.LiveTile.toString():
                return;
        }
    }

    private _defineDeletePopUp(): any {
        return (this.state.delete &&
            <Dialog
                isOpen={true}
                type={DialogType.largeHeader}
                onDismiss={this._delete.bind(this)}
                title='Are you sure you want to delete this Tile?'
                subText='If you delete this tile it will be removed from a the SharePoint list.'
                isBlocking={false}>
                <DialogFooter>
                    <Button buttonType={ButtonType.primary} onClick={this._delete.bind(this)}>Confirm</Button>
                    <Button onClick={this._showDeleteDialog.bind(this)}>Cancel</Button>
                </DialogFooter>
            </Dialog>);
    }

    private _defineTileBrand(): any {
        if (this.state.tile.FormFactor.toString() !== 'half')
            return (
                <div className="brand bg-dark opacity">
                    <span className="label">{this.state.tile.Label}</span>
                </div>);
    }

    private _defineTileActionIcons(): any {
        return (
            this.state.selected && this.props.edit && <div>
                <IconButton icon='Edit' onClick={this._edit.bind(this)}></IconButton>
                <IconButton icon='Delete' onClick={this._showDeleteDialog.bind(this)}></IconButton>
            </div>);
    }

    private _loadImages() {
        this.state.images = [];
        debugger;
        if (this.state.tile.PictureLibId != null)
            pnp.sp.web.lists.getById(this.state.tile.PictureLibId).items.select('EncodedAbsUrl').get().then((items: any[]) => {
                debugger;
                for (var i = 0; i < items.length; i++) {
                    this.state.images.push(
                        <div className = "tile-content image">
                        <img src={items[i].EncodedAbsUrl} alt='Image not loading...' />
                        </div>
                    );
                }
                this.setState(this.state);
            });
    }

    private _toggleClick() {
        this.state.selected = !this.state.selected;
        this.setState(this.state);
    }
}