import {ITile} from '../../Model/ITile';

export interface ITileProps {
    tile:ITile;
    action?: (props: any, target: string) => void;
    edit: boolean;
    delete:boolean;
}