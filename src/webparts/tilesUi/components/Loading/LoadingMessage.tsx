import * as React from 'react';
import { Spinner, Label, SpinnerSize} from 'office-ui-fabric-react';
import { ILoadingMessageProps } from './LoadingMessageProps';

export default class PPCustomDropDown extends React.Component<ILoadingMessageProps, void>{

    public render() {
        return (
            <div>
                <Label className={'center'}>{this.props.message}</Label>
                <Spinner size={3} />
            </div>
        );
    }

}