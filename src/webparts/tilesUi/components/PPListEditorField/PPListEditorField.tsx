import * as React from 'react';
import { IPPListFieldProps } from './PPListFieldProps';
import { TextField, PrimaryButton, Button } from 'office-ui-fabric-react';
import {ChoiceGroup, IChoiceGroupOption } from 'office-ui-fabric-react';
import * as strings from 'tilesUiStrings';



export interface IPPlistEditorFieldState {
    title: string;
    description: string;
}
export default class PPListEditor extends React.Component<IPPListFieldProps, IPPlistEditorFieldState>{

    public constructor(props) {
        super(props);
        this.state = { ...this.props.list };
    }

    public render() {
        return (
            <div>
                <TextField label={strings.PPListTitle}
                    onChanged={this._setTitle.bind(this)}
                />
                <TextField label={strings.PPListDescription}
                    onChanged={this._setDescription.bind(this)}
                />
                <PrimaryButton onClick={this._submit.bind(this)}>{strings.PPCreateListBTN}</PrimaryButton>
                <Button onClick={this._dismiss.bind(this)}>{strings.PPCancelBTN}</Button>                
            </div>
        );
    }

    private _submit() {
        this.props.action(this.state, 'Create List');
    }

    private _dismiss() {
        this.props.action();
    }
    
    private _setTitle(input: string): void {
        this.state.title = input;
    }

    private _setDescription(input: string): void {
        this.state.description = input;
    }

}