import {IList} from '../../PPaneListEditorField';

export interface IPPListFieldProps {
    list: IList;
    action: (params?: any, action?:string) => void;
}