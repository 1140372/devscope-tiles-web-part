import * as strings from 'tilesUiStrings';

export interface ITile{
    Id?: number;
    Label: string;
    Content: string;
    Ativo: boolean;
    Live: boolean;
    Color: string;
    PictureLibId: string;
    FormFactor: string;
    Animation: string;
    Type: string;
    URL: {
        Url: string;
        Description: string;
    };
}

export class formFactor {
    public constructor(public value: string) {
    }
    public toString() {
        return this.value;
    }
    // values 
    public static half = new formFactor(strings.TileFormFactorHalf);
    public static default = new formFactor(strings.TileFormFactorDefault);
    public static double = new formFactor(strings.TileFormFactorDouble);
    public static triple = new formFactor(strings.TileFormFactorTriple);
    public static quadro = new formFactor(strings.TileFormFactorQuadro);

    public static getOptionList() {
        
        return [
            { key: 'half', text: this.half.toString() },
            { key: 'default', text: this.default.toString() },
            { key: 'double', text: this.double.toString() },
            { key: 'triple', text: this.triple.toString() },
            { key: 'quadro', text: this.quadro.toString() }
        ];
    }
}

export class animation {
    public constructor(public value: string) {
    }
    public toString() {
        return this.value;
    }
    // values 
    public static slideLeft = new formFactor(strings.TileAnimationSlideLeft);
    public static slideRight = new formFactor(strings.TileAnimationSlideRight);
    public static slideLeftRight = new formFactor(strings.TileAnimationSlideLeftRight);
    public static slideUp = new formFactor(strings.TileAnimationSlideUp);
    public static slideDown = new formFactor(strings.TileAnimationSlideDown);
    public static slideUpDown = new formFactor(strings.TileAnimationSlideUpDown);

    public static getOptionList() {
        
        return [
            { key: 'slideLeft', text: this.slideLeft.toString() },
            { key: 'slideRight', text: this.slideRight.toString() },
            { key: 'slideLeftRight', text: this.slideLeftRight.toString() },
            { key: 'slideUp', text: this.slideUp.toString() },
            { key: 'slideDown', text: this.slideDown.toString() },
            { key: 'slideUpDown', text: this.slideUpDown.toString() }
        ];
    }
}

export class tileType {
    public constructor(public value: string) {
    }
    public toString() {
        return this.value;
    }
    // values 
    public static ImageTile = new tileType(strings.TileTypeImage);
    public static IconTile = new tileType(strings.TileTypeIcon);
    public static LiveTile = new tileType(strings.TileTypeLive);
    public static AnimatedTile = new tileType(strings.TileTypeAnimated);
    

    public static getOptionList() {
        return [
            { key: this.ImageTile.toString(), text: this.ImageTile.toString() },
            { key: this.IconTile.toString(), text: this.IconTile.toString() },
            { key: this.LiveTile.toString(), text: this.LiveTile.toString() },
            { key: this.AnimatedTile.toString(), text: this.AnimatedTile.toString() }
                        
        ];
    }
}