import {
    ListEnsureResult,
    Web,
    FieldAddResult,
} from 'sp-pnp-js';


export default class MyWeb extends Web {
    public ensureType() {
        this.lists.add('lista de teste', 'descrição', 1203, true,
            { ListTemplate: `<ListTemplate Name="Mylist"  Type="1103" BaseType="0"/>` }).catch((response: any) => { console.log(response); });
    }

    public ensureTileList(tileListName: string, description: string, action: () => void) {
        return this.lists.ensure(tileListName, description, 103).then((ler: ListEnsureResult) => {
            let queries = this._getQueries();
            if (ler.created) {
                ler.list.defaultView.get().then((view: any) => {
                    ler.list.getView(view.Id).fields.removeAll().then(_ => {
                        (function loop(i) {
                            ler.list.fields.createFieldAsXml(queries[i]).then((field: FieldAddResult) => {
                                ler.list.getView(view.Id).fields.add(field.data.InternalName)
                                    .then(_ => i > queries.length || loop(i + 1));
                            });
                        })(0);
                        ler.list.getView(view.Id).fields.add('URL');
                    });
                });
            }
            action();
        });
    }

    private _getQueries() {
        return [
            `<Field Name="TWP_Title" DisplayName="Label" Type="Text" Required="TRUE"/>`,
            `<Field Name="TWP_Ativo" DisplayName="Ativo" Type="Boolean" Required="TRUE" />`,
            `<Field Name="TWP_Live" DisplayName="Live" Type="Boolean" Required="TRUE"/>`,
            `<Field Name="TWP_Token" DisplayName="Token" Type="Text" Required="FALSE"/>`,
            `<Field Name="TWP_Icon" DisplayName="Content" Type="Text" Required="FALSE" />`,
            `<Field Name="TWP_Color" DisplayName="Color" Type="Text" Required="FALSE"/>`,
            `<Field Name="TWP_PicLib" DisplayName="PictureLibId" Type="Text" Required="FALSE"/>`,
            `<Field Name="TWP_TileType" DisplayName="Type" Type="Choice" Required="TRUE">
                <CHOICES>
                    <CHOICE>Icon</CHOICE>
                    <CHOICE>Image</CHOICE>
                    <CHOICE>Animated</CHOICE>
                    <CHOICE>Live</CHOICE>
                </CHOICES>
            </Field>`,
            `<Field Name="TWP_FormFactor" DisplayName="FormFactor" Type="Choice" Required="TRUE">
                <CHOICES>
                    <CHOICE>half</CHOICE>
                    <CHOICE>default</CHOICE>
                    <CHOICE>double</CHOICE>
                    <CHOICE>triple</CHOICE>
                    <CHOICE>quadro</CHOICE>
                </CHOICES>
            </Field>`,
            `<Field Name="TWP_Animation" DisplayName="Animation" Type="Choice" Required="FALSE">
                    <CHOICES>
                        <CHOICE>SlideUp</CHOICE>
                        <CHOICE>SlideDown</CHOICE>
                        <CHOICE>SlideUpDown</CHOICE>                
                        <CHOICE>SlideLeft</CHOICE>
                        <CHOICE>SlideRight</CHOICE>
                        <CHOICE>SlideLeftRight</CHOICE>
                    </CHOICES>
            </Field>`
        ];
    }
}

