import * as React from 'react';
import * as ReactDom from 'react-dom';
import {
    IPropertyPaneField,
    PropertyPaneFieldType,
    IPropertyPaneCustomFieldProps
} from '@microsoft/sp-webpart-base';
import PPListEditorField from './components/PPListEditorField/PPListEditorField';


export interface IList {
    title: string;
    description: string;
    baseTemplate: number;
}

export default class PPaneListEditorField implements IPropertyPaneField<IPropertyPaneCustomFieldProps>{
    public type: PropertyPaneFieldType = PropertyPaneFieldType.Custom;
    public targetProperty: string;
    public properties: any;
    private selectedList: IList;

    public constructor(targetProperty: string, properties: any) {
        this.targetProperty = targetProperty;
        this.properties = properties;
        this.properties.onRender = this.render.bind(this);
    }

    public render(elem: HTMLElement) {
        let element: any = React.createElement(PPListEditorField);
        if (this.properties.list) {
            element = React.createElement(PPListEditorField, {
                list: this.selectedList,
                action: this.properties.action
            });
        } 
        ReactDom.render(element, elem);
    }
}