import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version, DisplayMode } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneButton,
  PropertyPaneButtonType,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';
import { colors } from './colors';
import pnp, { ViewUpdateResult } from 'sp-pnp-js';
import TilesPanel from './components/TilesPanel/TilesPanel';
import { ILoadingMessageProps } from './components/Loading/LoadingMessageProps';
import LoadingMessage from './components/Loading/LoadingMessage';
import { ITilesPanelProps } from './components/TilesPanel/ITilesPanelProps';
import { ITilesUiWebPartProps } from './ITilesUiWebPartProps';
import PPaneTileField from './PPaneTileField';
import { ITile } from './Model/ITile';
import PPaneListEditorField from './PPaneListEditorField';
import PPaneCustomDropDownField from './PPaneCustomDropDown';
import cellRender from './components/PPCustomDropDown/CustomCellRenders';
import { SPComponentLoader } from '@microsoft/sp-loader';
import MyWeb from './Model/Web';
import * as strings from 'tilesUiStrings';
import "metro-dist/css/metro-bootstrap.css";



export default class TilesUiWebPart extends BaseClientSideWebPart<ITilesUiWebPartProps> {

  private options: any[];
  private group: any;

  public onInit(): Promise<void> {

    return super.onInit().then(_ => {
      pnp.setup({
        spfxContext: this.context,
        headers: {
          'Accept': 'application/json;odata=nometadata'
        }
      });
      SPComponentLoader.loadCss("https://devscopespfx.blob.core.windows.net/tiles/officeIcons.css");
      // SPComponentLoader.loadScript("https://devscopespfx.blob.core.windows.net/tiles/jquery.easing.1.3.min.js");
      // SPComponentLoader.loadScript("https://devscopespfx.blob.core.windows.net/tiles/jquery.min.js");
      // SPComponentLoader.loadScript("https://devscopespfx.blob.core.windows.net/tiles/jquery.mousewheel.js");
      // SPComponentLoader.loadScript("https://devscopespfx.blob.core.windows.net/tiles/jquery.widget.min.js");
      this.properties.dataLoadedFromSP = false;
      pnp.sp.web.as(MyWeb).ensureTileList('TestList', 'Lista de teste', this._getListsFromSP.bind(this)).then(_ => {
        this.properties.dataLoadedFromSP = true;
        this.render();
      });
    });
  }

  public render(): void {
    this.properties.editMode = (this.displayMode == DisplayMode.Edit);
    if (this.properties.dataLoadedFromSP) {
      const element: React.ReactElement<ITilesPanelProps> = React.createElement(
        TilesPanel, {
          title: this.properties.title,
          selectedList: this.properties.selectedList,
          editMode: this.properties.editMode,
          action: this._actionHandler.bind(this),
          bcolor: this.properties.bcolor,
          tiles: this.properties.currentTiles
        }
      );
      ReactDom.render(element, this.domElement);
    } else {
      const message: React.ReactElement<ILoadingMessageProps> = React.createElement(
        LoadingMessage, {
          message: strings.LoadingMessage,
          size: ''
        }
      );
      ReactDom.render(message, this.domElement);
    }

  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          groups: [
            this.group
          ]
        }
      ]
    };
  }

  protected onPropertyPaneConfigurationComplete() {
    this._setBasePPGroup();
  }

  protected onDispose() {
    this._setBasePPGroup();
  }

  //PROPERTYPPANE MANIPULATION//////////////////////////////////////////////////////////////////////////////  
  private _actionHandler(props?: any, panel?: string) {
    switch (panel) {
      case 'Tile Edit':
        this._setTileEditorGroup(props, 'Update Tile');
        break;
      case 'Tile':
        this._setTileEditorGroup(props, 'Create Tile');
        break;
      case 'List':
        this._setListEditorGroup();
        break;
      case 'Create Tile':
        this._newTile(props);
        break;
      case 'Update Tile':
        this._updateTile(props);
        break;
      case 'Delete':
        this._deleteTile(props);
        break;
      case 'Create List':
        this._newList(props);
        break;
      default:
        this._setBasePPGroup();
        break;
    }
    this.context.propertyPane.refresh();
    this.context.propertyPane.open();
  }

  private _setBasePPGroup() {
    this.group = {
      groupName: strings.PPBaseGroup,
      groupFields: [
        PropertyPaneTextField('title', {
          label: strings.PPPanelTitle
        }),
        new PPaneCustomDropDownField('bcolor', {
          label: strings.PPBgColor,
          dropDownOptions: colors,
          onSelectAction: this._setBcolor.bind(this),
          selectedItem: this.properties.bcolor,
          renderItem: cellRender.renderColorOption,
          renderSelectedItem: cellRender.renderColorOption
        }),
        new PPaneCustomDropDownField('selectedList', {
          label: strings.PPSelectList,
          dropDownOptions: this.options,
          onSelectAction: this._setList.bind(this),
          selectedItem: this.properties.selectedList,
          renderItem: cellRender.renderTextOption,
          renderSelectedItem: cellRender.renderTextOption
        }),
        PropertyPaneButton('prop', {
          text: strings.PPCreateListBTN,
          buttonType: PropertyPaneButtonType.Normal,
          onClick: () => this._actionHandler(undefined, 'List'),
        })
      ]
    };
  }

  private _setTileEditorGroup(tile, type: string) {
    this.group =
      {
        groupName: 'Tile Editor',
        groupFields: [
          new PPaneTileField({
            selectedTile: tile,
            type: type,
            propertyChanged: this.onPropertyPaneFieldChanged.bind(this),
            onSubmit: this._actionHandler.bind(this)
          })
        ]
      };
  }

  private _setListEditorGroup() {
    this.group =
      {
        groupName: 'List Editor',
        groupFields: [new PPaneListEditorField('currentList', {
          list: {},
          action: this._actionHandler.bind(this)
        })
        ]
      };
  }
  //CRUD TILES E LISTS//////////////////////////////////////////////////////////////////////////////////////

  private _getListsFromSP() {
    this.options = [];
    debugger;
    pnp.sp.web.lists.filter('BaseTemplate eq 103').get().then((lists: any[]) => {
    debugger;
      for (var i = 0; i < lists.length; i++) {
        this.options.push({
          key: lists[i].Id,
          text: lists[i].Title
        });
      }
      this._getItemForListFromSP();
      this._setBasePPGroup();
      this.context.propertyPane.refresh();
    });
  }

  private _getItemForListFromSP() {
    this.properties.currentTiles = [];
    if (this.properties.selectedList != null) {
      pnp.sp.web.lists.getById(this.properties.selectedList).items.get().then((items: ITile[]) => {
        for (var i = 0; i < items.length; i++) {
          this.properties.currentTiles.push(items[i]);
        }
        this.render();
      });
    }
  }

  private _newList(list: any) {
    pnp.sp.web.as(MyWeb).ensureTileList(list.title, list.description, this._getListsFromSP.bind(this));
  }

  private _newTile(Tile) {
    pnp.sp.web.lists.getById(this.properties.selectedList).items.add(Tile).then((response: any) => {
      this._getItemForListFromSP();
    });
  }

  private _updateTile(Tile) {
    pnp.sp.web.lists.getById(this.properties.selectedList).items.getById(Tile.Id).update(Tile).then((response: any) => {
      this._getItemForListFromSP();
    });
  }

  private _deleteTile(TileState) {
    pnp.sp.web.lists.getById(this.properties.selectedList).items.getById(TileState.tile.Id).delete().then((response: any) => {
      this._getItemForListFromSP();
    });
  }

  private _setBcolor(color) {
    this.properties.bcolor = color;
    this.render();
  }

  private _setList(list) {
    this.properties.selectedList = list;
    this._getItemForListFromSP();
  }

}


