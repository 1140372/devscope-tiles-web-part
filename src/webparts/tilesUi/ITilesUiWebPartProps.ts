import {ITile} from './Model/ITile';


export interface ITilesUiWebPartProps {
    title: string;
    bcolor:string;
    editMode: boolean;
    selectedItem: string;
    selectedList: string;
    currentTiles: ITile[];
    dataLoadedFromSP: boolean;
}
