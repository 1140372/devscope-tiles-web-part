import * as React from 'react';
import * as ReactDom from 'react-dom';
import {
    IPropertyPaneField,
    PropertyPaneFieldType,
    IPropertyPaneCustomFieldProps
} from '@microsoft/sp-webpart-base';
import PPCustomDropDown from './components/PPCustomDropDown/PPaneCustomDropDown';
import { IPCustomDropDownProps } from './components/PPCustomDropDown/PPaneCDropDownProps';

export default class PPaneCustomDropDownField implements IPropertyPaneField<IPropertyPaneCustomFieldProps>{
    public type: PropertyPaneFieldType = PropertyPaneFieldType.Custom;
    public targetProperty: string;
    public properties: any;


    public constructor(targetProperty: string, properties: IPCustomDropDownProps) {
        
        this.targetProperty = targetProperty;
        this.properties = properties;
        this.properties.onRender = this.render.bind(this);
    }

    public render(elem: HTMLElement) {
        const element: React.ReactElement<IPCustomDropDownProps> = React.createElement(PPCustomDropDown, {
            label: this.properties.label,
            dropDownOptions: this.properties.dropDownOptions,
            selectedItem:this.properties.selectedItem,
            onSelectAction: this.properties.onSelectAction,
            renderSelectedItem: this.properties.renderSelectedItem,
            renderItem: this.properties.renderItem
        });
        ReactDom.render(element, elem);
    }

}

