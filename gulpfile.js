'use strict';

const gulp = require('gulp');
const build = require('@microsoft/sp-build-web');
const spsync = require('gulp-spsync-creds').sync;

build.task('upload-app-pkg', {
    execute: (config) => {
        return new Promise((resolve, reject) => {
            const pkgFile = require('./config/package-solution.json');
            const folderLocation = `./sharepoint/${pkgFile.paths.zippedPackage}`;
            
            return gulp.src(folderLocation)
            .pipe(spsync({
                "username": "nunobarbosa@nboffice365dev.onmicrosoft.com",
                "password": "GErMN5Ao",
                "site": "https://nboffice365dev.sharepoint.com/sites/spfdev/",
                "libraryPath": "AppCatalog",
                "publish": true
            }))
            .on('finish', resolve);
        });
    }
});

build.initialize(gulp);
